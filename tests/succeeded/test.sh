#!/usr/bin/env bash
# shellcheck shell=bash

set -x

TZ="Europe/Zurich"

# פונקציה להדפסת "שלום עולם"
function hello_world() {
    echo "שלום עולם"
}

# פונקציה לבדיקת האם קובץ קיים
function file_exists() {
    if [ -e "$1" ]; then
        echo "הקובץ $1 קיים."
    else
        echo "הקובץ $1 לא קיים."
    fi
}

# פונקציה לבדיקת האם מספר זוגי
function is_even() {
    if (($1 % 2 == 0)); then
        echo "$1 הוא מספר זוגי."
    else
        echo "$1 הוא מספר אי זוגי."
    fi
}

# פונקציה לחיבור של שני מחרוזות
function concatenate_strings() {
    concatenated="$1$2"
    echo "שני המחרוזות מחוברות: $concatenated"
}

# קריאה לפונקציות שונות
hello_world

file_exists "example.txt"
file_exists "not_existing.txt"

is_even 4
is_even 5

concatenate_strings "שלום, " "עולם!"
